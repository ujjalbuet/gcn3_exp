package main

import (
	"fmt"
	"sync"

	"gitlab.com/akita/gcn3/benchmarks/heteromark/fir"
	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/gcn3/platform"
)

func main() {
	engine, gpuDriver := platform.BuildNR9NanoPlatform(4)
	gpuDriver.Run()

	kernelTimeCounter := driver.NewKernelTimeCounter()
	gpuDriver.AcceptHook(kernelTimeCounter)

	b1 := fir.NewBenchmark(gpuDriver)
	b1.SelectGPU([]int{1})
	b1.Length = 65536

	b2 := fir.NewBenchmark(gpuDriver)
	b2.SelectGPU([]int{2})
	b2.Length = 65536

	var wg sync.WaitGroup
	wg.Add(2)

	go func(wg *sync.WaitGroup) {
		b1.Run()
		wg.Done()
	}(&wg)

	go func(wg *sync.WaitGroup) {
		b2.Run()
		wg.Done()
	}(&wg)

	wg.Wait()

	fmt.Printf("Kernel Time %.15f\n", kernelTimeCounter.TotalTime)
	fmt.Printf("Total Time: %.15f\n", engine.CurrentTime())
}
