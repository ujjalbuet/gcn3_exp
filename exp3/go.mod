module gitlab.com/syifan/gcn3_exp/exp3

require (
	gitlab.com/akita/akita v1.3.2
	gitlab.com/akita/gcn3 v1.4.1
	gitlab.com/akita/mem v1.2.2
	gitlab.com/akita/noc v1.1.3
	gitlab.com/akita/vis v0.2.0
)

//replace gitlab.com/akita/mem => /Users/Ujjal/TSM/Saif_mem/mem

go 1.12
