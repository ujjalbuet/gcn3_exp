package main

import (
	"fmt"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/mem"

	//"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/mem/vm/mmu"
	"gitlab.com/akita/noc"
	//"gitlab.com/akita/vis/trace"
)

//BuildNR9NanoTSMPlatform creates a platform that equips with several R9Nano
//GPUs
func BuildNR9NanoTSMPlatform(
	numGPUs int,
) (
	akita.Engine,
	*driver.Driver,
) {
	var engine akita.Engine

	engine = akita.NewSerialEngine()

	mmuBuilder := mmu.MakeBuilder().
		WithEngine(engine).
		WithFreq(1 * akita.GHz)
	mmuComponent := mmuBuilder.Build("MMU")
	gpuDriver := driver.NewDriver(engine, mmuComponent)

	//	mmu := vm.NewMMU("MMU", engine, &vm.DefaultPageTableFactory{})
	//	mmu.Latency = 100
	//  mmu.ShootdownLatency = 50
	//	gpuDriver := driver.NewDriver(engine, mmu)
	//connection := akita.NewDirectConnection(engine)
	connection := noc.NewFixedBandwidthConnection(32, engine, 1*akita.GHz)
	connection.SrcBufferCapacity = 40960000

	memCtrls := createTSMMemoryControllers(engine, connection)
	lowModuleFinderForL2 := cache.NewInterleavedLowModuleFinder(4096)
	for _, m := range memCtrls {
		lowModuleFinderForL2.LowModules = append(
			lowModuleFinderForL2.LowModules, m.ToTop)
	}

	gpuBuilder := NewR9NanoGPUBuilder().
		WithEngine(engine).
		WithExternalConn(connection).
		WithMMU(mmuComponent).
		WithNumCUPerShaderArray(4).
		WithNumShaderArray(16).
		WithNumMemoryBank(8)
	gpuBuilder.LowModuleFinderForL2 = lowModuleFinderForL2

	for i := 1; i < numGPUs+1; i++ {
		name := fmt.Sprintf("GPU_%d", i)
		memAddrOffset := uint64(i) * 4 * mem.GB
		gpu := gpuBuilder.
			WithMemAddrOffset(memAddrOffset).
			Build(name, uint64(i))
		gpuDriver.RegisterGPU(gpu, 4*mem.GB)
		gpu.Driver = gpuDriver.ToGPUs
	}

	connection.PlugIn(gpuDriver.ToGPUs)
	connection.PlugIn(mmuComponent.ToTop)

	return engine, gpuDriver
}

func createTSMMemoryControllers(
	engine akita.Engine,
	conn akita.Connection,
) []*mem.IdealMemController {
	numMemCtrl := 32
	memCtrls := make([]*mem.IdealMemController, 0)

	for i := 0; i < numMemCtrl; i++ {
		memCtrl := mem.NewIdealMemController(
			fmt.Sprintf("TSM.DRAM_%d", i),
			engine,
			32*mem.GB)
		memCtrls = append(memCtrls, memCtrl)

		addrConverter := mem.InterleavingConverter{
			InterleavingSize:    4096,
			TotalNumOfElements:  numMemCtrl,
			CurrentElementIndex: i,
			Offset:              4 * mem.GB,
		}
		memCtrl.AddressConverter = addrConverter

		conn.PlugIn(memCtrl.ToTop)
	}

	return memCtrls
}
