package main

import (
	"flag"

	"gitlab.com/akita/gcn3/benchmarks/heteromark/fir"
)

func main() {
	flag.Parse()

	runner := Runner{}
	runner.Init()

	benchmark := fir.NewBenchmark(runner.GPUDriver)
	benchmark.Length = 4096

	runner.AddBenchmark(benchmark)

	runner.Run()
}
