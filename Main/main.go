package main

import (
	"flag"
)

func main() {
	flag.Parse()

	runner := new(Runner).ParseFlag().Init()
	//runner.Init()
	benchmark := GetWorkload(runner.GPUDriver)
	//benchmark := fir.NewBenchmark(runner.GPUDriver)
	//benchmark.Width = 32768 //262144 //32768

	runner.AddBenchmark(benchmark)

	runner.Run()
}
