// Package platform provides predefined platform definitions.
package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/mem/idealmemcontroller"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/mem/vm/mmu"

	"gitlab.com/akita/mgpusim"
	"gitlab.com/akita/mgpusim/driver"
	"gitlab.com/akita/noc/networking/pcie"
	"gitlab.com/akita/util/tracing"
)

var DebugISA bool
var TraceVis bool
var TraceMem bool
var memBankInterleaving uint64 = 4096
var NumShader = 8
var NumMemoryBank = 8

//nolint:gocyclo,funlen
//BuildNR9NanoPlatform creates a platform that equips with several R9Nano GPUs
func BuildNR9NanoTSMPlatform(
	numGPUs int,
) (
	akita.Engine,
	*driver.Driver,
) {
	var engine akita.Engine

	engine = akita.NewSerialEngine()

	engine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))

	pageTable := vm.NewPageTable(12)
	mmuBuilder := mmu.MakeBuilder().
		WithEngine(engine).
		WithFreq(1 * akita.GHz).
		WithLog2PageSize(12).
		WithPageTable(pageTable)
	mmuComponent := mmuBuilder.Build("MMU")

	gpuDriver := driver.NewDriver(engine, pageTable, 12)

	// connection := akita.NewDirectConnection("ExternalConn", engine, 1*akita.GHz)
	//connection := noc.NewFixedBandwidthConnection(32, engine, 1*akita.GHz)
	//connection.SrcBufferCapacity = 40960000

	pcieConnector := pcie.NewConnector().
		WithEngine(engine).
		WithVersion4().
		WithX16().
		WithSwitchLatency(1).
		WithNetworkName("PCIe")
	pcieConnector.CreateNetwork()
	rootComplexID := pcieConnector.CreateRootComplex(
		[]akita.Port{
			gpuDriver.ToGPUs,
			gpuDriver.ToMMU,
			mmuComponent.MigrationPort,
			mmuComponent.ToTop,
		})

	//mmuComponent.MigrationServiceProvider = gpuDriver.ToMMU
	memCtrls := createTSMMemoryControllers(engine, numGPUs)
	for _, memCtrl := range memCtrls {
		pcieConnector.PlugInDevice(rootComplexID, []akita.Port{memCtrl.ToTop})
	}

	lowModuleFinderForL2 := cache.NewInterleavedLowModuleFinder(memBankInterleaving)
	for _, m := range memCtrls {
		lowModuleFinderForL2.LowModules = append(
			lowModuleFinderForL2.LowModules, m.ToTop)
	}

	gpuBuilder := MakeR9NanoGPUBuilder().
		WithEngine(engine).
		WithMMU(mmuComponent).
		WithNumCUPerShaderArray(4).
		WithNumShaderArray(NumShader).
		WithNumMemoryBank(NumMemoryBank).
		WithLog2PageSize(12)
	gpuBuilder.LowModuleFinderForL2 = lowModuleFinderForL2

	//lastSwitchID := rootComplexID
	for i := 1; i < numGPUs+1; i++ {
		//if i%2 == 1 {
		//	lastSwitchID = pcieConnector.AddSwitch(lastSwitchID)
		//}

		name := fmt.Sprintf("GPU_%d", i)
		memAddrOffset := uint64(i) * 4 * mem.GB
		gpu := gpuBuilder.
			WithMemAddrOffset(memAddrOffset).
			Build(name, uint64(i))
		gpuDriver.RegisterGPU(gpu, 4*mem.GB)
		gpu.CommandProcessor.Driver = gpuDriver.ToGPUs

		pcieConnector.PlugInDevice(rootComplexID, gpuExternalPorts(gpu))
	}

	if TraceVis {
		tracer := tracing.NewMongoDBTracer()
		tracer.Init()
		tracing.CollectTrace(gpuDriver, tracer)

		gpuBuilder = gpuBuilder.WithVisTracer(tracer)
	}

	return engine, gpuDriver
}

func gpuExternalPorts(gpu *mgpusim.GPU) []akita.Port {
	ports := make([]akita.Port, 0)

	ports = append(ports, gpu.CommandProcessor.ToDriver)
	ports = append(ports,
		gpu.CommandProcessor.DMAEngine.Component().(*mgpusim.DMAEngine).ToMem)
	for _, tlb := range gpu.L2TLBs {
		ports = append(ports, tlb.BottomPort)
	}
	for _, l2 := range gpu.L2Caches {
		ports = append(ports, l2.BottomPort)
	}

	return ports
}

func createTSMMemoryControllers(
	engine akita.Engine, numGPUs int,
) []*idealmemcontroller.Comp {
	numMemCtrl := 8 * numGPUs
	memCtrls := make([]*idealmemcontroller.Comp, 0)

	for i := 0; i < numMemCtrl; i++ {
		memCtrl := idealmemcontroller.New(
			fmt.Sprintf("TSM.DRAM_%d", i),
			engine,
			8*uint64(numGPUs)*mem.GB)
		memCtrls = append(memCtrls, memCtrl)

		addrConverter := idealmemcontroller.InterleavingConverter{
			InterleavingSize:    memBankInterleaving,
			TotalNumOfElements:  numMemCtrl,
			CurrentElementIndex: i,
			Offset:              4 * mem.GB,
		}
		memCtrl.AddressConverter = addrConverter

		// conn.PlugIn(memCtrl.ToTop, 1)
	}

	return memCtrls
}
