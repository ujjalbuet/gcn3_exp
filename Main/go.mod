//module gitlab.com/syifan/gcn3_exp/exp3
module /Users/Ujjal/TSM/TSM-All-Experoments-ISCA-2020/TSM-WT-NC-BW/Main

// require (
// 	github.com/tebeka/atexit v0.1.0
// 	gitlab.com/akita/akita v1.9.1
// 	gitlab.com/akita/gcn3 v1.6.0
// 	gitlab.com/akita/mem v1.7.1
// 	//gitlab.com/akita/mem v1.7.1
// 	gitlab.com/akita/noc v1.3.3
// 	gitlab.com/akita/util v0.2.0
// 	gitlab.com/akita/vis v0.2.0
// 	gitlab.com/ujjalbuet/gcn3_exp/exp4 v0.0.0-00010101000000-000000000000

// //gitlab.com/ujjalbuet/gcn3_exp v0.0.0-20190827132837-f8ea2b22ad11

//)

require (
	github.com/cosiner/argv v0.0.1 // indirect
	github.com/go-delve/delve v1.4.0 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/peterh/liner v1.2.0 // indirect
	github.com/sirupsen/logrus v1.5.0 // indirect
	github.com/spf13/cobra v0.0.6 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/tebeka/atexit v0.3.0
	gitlab.com/akita/akita v1.10.1
	gitlab.com/akita/mem v1.8.3
	gitlab.com/akita/mgpusim v1.7.1
	//gitlab.com/akita/mem v1.7.1
	gitlab.com/akita/noc v1.4.0
	gitlab.com/akita/util v0.5.0
	gitlab.com/akita/vis v0.2.0
	go.starlark.net v0.0.0-20200306205701-8dd3e2ee1dd5 // indirect
	golang.org/x/arch v0.0.0-20200312215426-ff8b605520f4 // indirect
//gitlab.com/ujjalbuet/gcn3_exp v0.0.0-20190827132837-f8ea2b22ad11

)

//correct replacement for Coherency
replace gitlab.com/akita/mgpusim => ../mgpusim

//replace gitlab.com/akita/util => ../util

//replace gitlab.com/ujjalbuet/gcn3_exp/exp4 => ../exp4

go 1.13
