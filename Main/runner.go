// Package runner defines how default benchmark samples are executed.
package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"sync"

	"github.com/tebeka/atexit"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mgpusim"
	"gitlab.com/akita/mgpusim/benchmarks"
	"gitlab.com/akita/mgpusim/driver"

	"gitlab.com/akita/mgpusim/timing"
	"gitlab.com/akita/util/tracing"
)

//var timingFlag = flag.Bool("timing", false, "Run detailed timing simulation.")
//var parallelFlag = flag.Bool("parallel", false, "Run the simulation in parallel.")
var memTracing = flag.Bool("trace-mem", false, "Generate memory trace")
var cacheLatencyReportFlag = flag.Bool("report-cache-latency", true, "Report the average cache latency.")
var gpuFlag = flag.String("gpus", "1",
	"The GPUs to use, use a format like 1,2,3,4")

var instSimulated = flag.Uint64("ins_to_simulate", 10000000, "Max instructions to simulate")

var numGPUs = flag.Int("num-gpus", 1, "The number of GPUs to use")

//var useUnifiedModel = flag.Bool("unified", false,
//	"Run benchmark with Unified GPU Model")

type cacheLatencyTracer struct {
	tracer *tracing.AverageTimeTracer
	cache  akita.Component
}

// Runner is a class that helps running the benchmarks in the official samples.
type Runner struct {
	Engine                  akita.Engine
	GPUDriver               *driver.Driver
	KernelTimeCounter       *tracing.BusyTimeTracer
	PerGPUKernelTimeCounter []*tracing.BusyTimeTracer
	CacheLatencyTracers     []cacheLatencyTracer
	Benchmarks              []benchmarks.Benchmark
	//Timing                  bool
	//Verify                  bool
	//Parallel                bool
	ReportCacheLatency bool
	useUnifiedModel    bool
	//UseUnifiedMemory        bool

	//GPUIDs []int

	MaxInsSimulated uint64
	KernelTime      io.Writer
}

// ParseFlag applies the runner flag to runner object
func (r *Runner) ParseFlag() *Runner {

	// if *cacheLatencyReportFlag {
	r.ReportCacheLatency = true
	// }

	r.MaxInsSimulated = *instSimulated
	//timing.MaxInst = *instSimulated

	//r.parseGPUFlag()
	return r
}

func (r *Runner) startProfilingServer() {
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		panic(err)
	}

	fmt.Println("Profiling server running on:", listener.Addr().(*net.TCPAddr).Port)

	panic(http.Serve(listener, nil))
}

func (r *Runner) reportStats() {
	now := r.Engine.CurrentTime()
	r.KernelTimeCounter.TerminateAllTasks(now)
	fmt.Printf("Kernel time: %.12f\n", r.KernelTimeCounter.BusyTime())
	fmt.Printf("Total time: %.12f\n", now)
	for i, c := range r.PerGPUKernelTimeCounter {
		c.TerminateAllTasks(now)
		fmt.Printf("GPU %d kernel time: %.12f\n", i+1, c.BusyTime())
	}
	for _, tracer := range r.CacheLatencyTracers {
		fmt.Printf("Cache %s average latency %.12f\n",
			tracer.cache.Name(),
			tracer.tracer.AverageTime(),
		)
	}
	//fmt.Printf("Traffic: %d\n", r.trafficCounter.TotalData)
	//r.addL1ReqCounter()
	//r.addL2ReqCounter()
	//r.addMemReqCounter()
	f, _ := os.Create("kernel_time")
	r.KernelTime = f
	fmt.Fprintf(r.KernelTime, "Kernel time: %.12f\n", r.KernelTimeCounter.BusyTime())
	fmt.Fprintf(r.KernelTime, "Total time: %.12f\n", r.Engine.CurrentTime())
	//fmt.Fprintf(r.KernelTime, "Traffic: %d\n", r.trafficCounter.TotalData)
}

func (r *Runner) unifyMultiGPUs(gpus []*mgpusim.GPU) {
	dispatcher := gpus[0].Dispatchers[0].(*mgpusim.Dispatcher)
	conn := gpus[0].InternalConnection
	dispatcher.CUs = nil
	for i := 0; i < NumShader*4; i++ {
		for j := 0; j < len(gpus); j++ {
			gpu := gpus[j]
			cu := gpu.CUs[i].(*timing.ComputeUnit)
			dispatcher.RegisterCU(cu.ToACE)
			conn.PlugIn(cu.ToACE, 1)
		}
	}
}

// Init initializes the platform simulate
func (r *Runner) Init() *Runner {
	go r.startProfilingServer()

	log.SetFlags(log.Llongfile)

	//UseParallelEngine = true

	//r.Engine, r.GPUDriver = BuildNR9NanoTSMPlatform(len(r.GPUIDs))
	r.Engine, r.GPUDriver = BuildNR9NanoTSMPlatform(*numGPUs)
	r.unifyMultiGPUs(r.GPUDriver.GPUs)
	//r.GPUDriver.GPUs = []*gcn3.GPU{r.GPUDriver.GPUs[0]}
	//i := 1; i < numGPUs+1; i++ {
	//	r.GPUDriver.GPUs = append(r.GPUDriver.GPUs, []*gcn3.GPU{r.GPUDriver.GPUs[i]})
	//}

	r.addKernelTimeTracer()
	r.addCacheLatencyTracer()

	atexit.Register(func() { r.reportStats() })

	return r
}

func (r *Runner) addKernelTimeTracer() {
	r.KernelTimeCounter = tracing.NewBusyTimeTracer(
		func(task tracing.Task) bool {
			return task.What == "*driver.LaunchKernelCommand"
		})
	tracing.CollectTrace(r.GPUDriver, r.KernelTimeCounter)

	for _, gpu := range r.GPUDriver.GPUs {
		gpuKernelTimeCountner := tracing.NewBusyTimeTracer(
			func(task tracing.Task) bool {
				return task.What == "*mgpusim.LaunchKernelReq"
			})
		r.PerGPUKernelTimeCounter = append(
			r.PerGPUKernelTimeCounter, gpuKernelTimeCountner)
		tracing.CollectTrace(gpu.CommandProcessor, gpuKernelTimeCountner)
	}
}

func (r *Runner) addCacheLatencyTracer() {
	if !r.ReportCacheLatency {
		return
	}

	//for _, gpu := range r.GPUDriver.GPUs {
	for _, gpu := range r.GPUDriver.GPUs {
		for _, cache := range gpu.L2Caches {
			tracer := tracing.NewAverageTimeTracer(
				func(task tracing.Task) bool {
					return task.Kind == "req_in"
				})
			r.CacheLatencyTracers = append(r.CacheLatencyTracers,
				cacheLatencyTracer{tracer: tracer, cache: cache})
			tracing.CollectTrace(cache, tracer)
		}
	}
}

// func (r *Runner) parseGPUFlag() {
// 	r.GPUIDs = make([]int, 0)
// 	gpuIDTokens := strings.Split(*gpuFlag, ",")
// 	for _, t := range gpuIDTokens {
// 		gpuID, err := strconv.Atoi(t)
// 		if err != nil {
// 			log.Fatal(err)
// 		}
// 		r.GPUIDs = append(r.GPUIDs, gpuID)
// 	}
// }

// AddBenchmark adds an benchmark that the driver runs
func (r *Runner) AddBenchmark(b benchmarks.Benchmark) {
	//b.SelectGPU(r.GPUIDs)
	b.SelectGPU([]int{1})
	r.Benchmarks = append(r.Benchmarks, b)
}

// AddBenchmarkWithoutSettingGPUsToUse allows for user specified GPUs for
// the benchmark to run.
func (r *Runner) AddBenchmarkWithoutSettingGPUsToUse(b benchmarks.Benchmark) {
	r.Benchmarks = append(r.Benchmarks, b)
}

// Run runs the benchmark on the simulator
func (r *Runner) Run() {
	r.GPUDriver.Run()

	var wg sync.WaitGroup
	for _, b := range r.Benchmarks {
		wg.Add(1)
		go func(b benchmarks.Benchmark, wg *sync.WaitGroup) {
			b.Run()
			wg.Done()
		}(b, &wg)
	}
	wg.Wait()

	r.GPUDriver.Terminate()
	r.Engine.Finished()

	//fmt.Printf("Kernel time: %.12f\n", r.KernelTimeCounter.BusyTime())
	//fmt.Printf("Total time: %.12f\n", r.Engine.CurrentTime())
	//	for i, c := range r.PerGPUKernelTimeCounter {
	//		fmt.Printf("GPU %d kernel time: %.12f\n", i+1, c.BusyTime())
	//	}

	atexit.Exit(0)
}

// func (r *Runner) addL1ReqCounter() {
// 	totalReq := l1v.NumReqsToBottom
// 	fmt.Printf("Total requests from L1 to L2: %d \n", totalReq)
// }
// func (r *Runner) addL2ReqCounter() {
// 	totalReq := l2v.NumReqsToBottomL2
// 	fmt.Printf("Total requests from L2 to MM: %d \n", totalReq)
// }

// func (r *Runner) addMemReqCounter() {
// 	totalReq := idealmemcontroller.NumReqRec
// 	fmt.Printf("Total requests Received in Main Memory: %d \n", totalReq)
// }
