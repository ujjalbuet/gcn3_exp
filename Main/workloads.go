package main

import (
	"flag"
	"log"

	"gitlab.com/akita/mgpusim/benchmarks"
	"gitlab.com/akita/mgpusim/benchmarks/amdappsdk/bitonicsort"
	"gitlab.com/akita/mgpusim/benchmarks/amdappsdk/fastwalshtransform"
	"gitlab.com/akita/mgpusim/benchmarks/amdappsdk/floydwarshall"
	"gitlab.com/akita/mgpusim/benchmarks/amdappsdk/matrixmultiplication"
	"gitlab.com/akita/mgpusim/benchmarks/amdappsdk/matrixtranspose"
	"gitlab.com/akita/mgpusim/benchmarks/amdappsdk/simpleconvolution"
	"gitlab.com/akita/mgpusim/benchmarks/dnn/maxpooling"
	"gitlab.com/akita/mgpusim/benchmarks/dnn/relu"
	"gitlab.com/akita/mgpusim/benchmarks/heteromark/aes"
	"gitlab.com/akita/mgpusim/benchmarks/heteromark/fir"
	"gitlab.com/akita/mgpusim/benchmarks/heteromark/kmeans"
	"gitlab.com/akita/mgpusim/benchmarks/heteromark/pagerank"
	"gitlab.com/akita/mgpusim/benchmarks/polybench/atax"
	"gitlab.com/akita/mgpusim/benchmarks/polybench/bicg"
	"gitlab.com/akita/mgpusim/benchmarks/shoc/bfs"
	"gitlab.com/akita/mgpusim/benchmarks/shoc/stencil2d"
	"gitlab.com/akita/mgpusim/driver"
	// "gitlab.com/ujjalbuet/gcn3_exp/exp4/mrsw"
	// "gitlab.com/ujjalbuet/gcn3_exp/exp4/xtreme1"
	// "gitlab.com/ujjalbuet/gcn3_exp/exp4/xtreme2"
	// "gitlab.com/ujjalbuet/gcn3_exp/exp4/xtreme3"
	//"gitlab.com/ujjalbuet/gcn3_exp/exp4/mrsw_rm"
	//"gitlab.com/ujjalbuet/gcn3_exp/exp4/nrnw"
)

var benchmarkName = flag.String("benchmark", "fir", "The benchmark to run")

//GetWorkload gets the workload to run
func GetWorkload(d *driver.Driver) benchmarks.Benchmark {
	switch *benchmarkName {
	// case "xtreme3":
	// 	b := xtreme3.NewBenchmark(d)
	// 	b.Width = 16384 //131072 //8388608 //2097152 //10240 //8192 //1048576 //65536//16384 //1048576
	// 	//b.NY = 4096 //10240
	// 	return b
	// case "xtreme2":
	// 	b := xtreme2.NewBenchmark(d)
	// 	b.Width = 131072 //26384 //8388608 //2097152 //10240 //8192 //1048576 //65536//16384 //1048576
	// 	//b.NY = 4096 //10240
	// 	return b
	// case "xtreme1":
	// 	b := xtreme1.NewBenchmark(d)
	// 	b.Width = 16384 //8388608 //2097152 //10240 //8192 //1048576 //65536//16384 //1048576
	// 	//b.NY = 4096 //10240
	// 	return b
	// case "mrsw":
	// 	b := mrsw.NewBenchmark(d)
	// 	b.Width = 1024 //10240 //8192 //1048576 //65536//16384 //1048576
	// 	//b.NY = 4096 //10240
	// 	return b

	case "atax":
		b := atax.NewBenchmark(d)
		b.NX = 4096 //10240 //8192 //1048576 //65536//16384 //1048576
		b.NY = 4096 //10240
		return b
	case "bicg":
		b := bicg.NewBenchmark(d)
		b.NX = 4096 //8192 //1048576 //65536//16384 //1048576
		b.NY = 4096
		return b
	case "fwt":
		b := fastwalshtransform.NewBenchmark(d)
		b.Length = 8388608 //8810000 //140960000 //8192 //1048576 //65536//16384 //1048576
		return b

	case "st":
		b := stencil2d.NewBenchmark(d)
		b.NumRows = 2048 //4096 //8192
		b.NumCols = 2048 //4096 //8192
		b.NumIteration = 5
		//1048576 //65536//16384 //1048576
		return b
	case "fws":
		b := floydwarshall.NewBenchmark(d)
		b.NumNodes = 2048 //4096 //8192 //1048576 //65536//16384 //1048576
		return b
	case "aes":
		b := aes.NewBenchmark(d)
		b.Length = 70960000 //1048576 //65536//16384 //1048576
		return b
	case "bs", "bitonicsort":
		b := bitonicsort.NewBenchmark(d)
		b.Length = 17620000 //140960000
		return b
	case "fir":
		b := fir.NewBenchmark(d)
		b.Length = 4096 //8810000 //70960000 //65536 //8096000 //65536 //65536//16384
		return b
	case "km", "kmeans":
		b := kmeans.NewBenchmark(d)
		b.NumPoints = 251200 //502400 //2009600 //1024//256
		b.NumFeatures = 32
		b.NumClusters = 16 //8192
		b.MaxIter = 20
		return b
	case "mm", "matrixmultiplication":
		b := matrixmultiplication.NewBenchmark(d)
		b.X = 4096 //7168
		b.Y = 4096 //7168
		b.Z = 4096 //7168
		return b
	case "mt", "matrixtranspose":
		b := matrixtranspose.NewBenchmark(d)
		b.Width = 4096 //8192
		return b
	case "mp", "maxpooling":
		b := maxpooling.NewBenchmark(d, 1, 1, 8192, 8192)
		return b
	case "rl", "relu":
		b := relu.NewBenchmark(d)
		b.Length = 17620000 //8096000 //1048576 //8096000 //65536 //16384 //1048576
		return b
	case "sc", "simpleconvolution":
		b := simpleconvolution.NewBenchmark(d)
		b.Width = 4096  //8192  //1024 //256
		b.Height = 4096 //8192 //1024 //256
		b.SetMaskSize(256)
		return b

	case "bfs":
		b := bfs.NewBenchmark(d)
		b.NumNode = 150000 //150000 //8192  //1024 //256
		b.Degree = 1000    //1000    //8192 //1024 //256ç
		b.MaxDepth = 10
		return b

	case "pr":
		b := pagerank.NewBenchmark(d)
		b.NumNodes = 2048                              //247680                            //8192  //1024 //256
		b.NumConnections = b.NumNodes * b.NumNodes * 1 //b.NumNodes //8192 //1024 //256
		b.MaxIterations = 10
		return b
	default:
		log.Panicf("unknown benchmark name %s\n", *benchmarkName)
	}
	panic("never")
}
