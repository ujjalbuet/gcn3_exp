#define ALPHA 2
#define BETA 3

// Calculate A = B + C by all GPUs
__kernel void mrsw_read(__global float4 *VecA,
                              __global float4 *VecB,
                              __global float4* VecC,
                              int widthA)
{
    int gid = get_global_id(0);

    if (gid < widthA)
        VecC[gid] = ALPHA * VecA[gid] + BETA * VecB[gid];

}

//write to A by GPU0

__kernel void mrsw_write(__global float4 *VecX,
                              __global float4 *VecC1,
                              __global float4* VecC2,
                              int widthA)
{
    int gid = get_global_id(0);

    if (gid < widthA)
        VecX[gid] = VecC1[gid] + VecC2[gid];

}
