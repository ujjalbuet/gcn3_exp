// Code generated by "esc -o bindata.go -pkg clmw -private kernels.hsaco"; DO NOT EDIT.

package clmw

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"sync"
	"time"
)

type _escLocalFS struct{}

var _escLocal _escLocalFS

type _escStaticFS struct{}

var _escStatic _escStaticFS

type _escDirectory struct {
	fs   http.FileSystem
	name string
}

type _escFile struct {
	compressed string
	size       int64
	modtime    int64
	local      string
	isDir      bool

	once sync.Once
	data []byte
	name string
}

func (_escLocalFS) Open(name string) (http.File, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	return os.Open(f.local)
}

func (_escStaticFS) prepare(name string) (*_escFile, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	var err error
	f.once.Do(func() {
		f.name = path.Base(name)
		if f.size == 0 {
			return
		}
		var gr *gzip.Reader
		b64 := base64.NewDecoder(base64.StdEncoding, bytes.NewBufferString(f.compressed))
		gr, err = gzip.NewReader(b64)
		if err != nil {
			return
		}
		f.data, err = ioutil.ReadAll(gr)
	})
	if err != nil {
		return nil, err
	}
	return f, nil
}

func (fs _escStaticFS) Open(name string) (http.File, error) {
	f, err := fs.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.File()
}

func (dir _escDirectory) Open(name string) (http.File, error) {
	return dir.fs.Open(dir.name + name)
}

func (f *_escFile) File() (http.File, error) {
	type httpFile struct {
		*bytes.Reader
		*_escFile
	}
	return &httpFile{
		Reader:   bytes.NewReader(f.data),
		_escFile: f,
	}, nil
}

func (f *_escFile) Close() error {
	return nil
}

func (f *_escFile) Readdir(count int) ([]os.FileInfo, error) {
	if !f.isDir {
		return nil, fmt.Errorf(" escFile.Readdir: '%s' is not directory", f.name)
	}

	fis, ok := _escDirs[f.local]
	if !ok {
		return nil, fmt.Errorf(" escFile.Readdir: '%s' is directory, but we have no info about content of this dir, local=%s", f.name, f.local)
	}
	limit := count
	if count <= 0 || limit > len(fis) {
		limit = len(fis)
	}

	if len(fis) == 0 && count > 0 {
		return nil, io.EOF
	}

	return fis[0:limit], nil
}

func (f *_escFile) Stat() (os.FileInfo, error) {
	return f, nil
}

func (f *_escFile) Name() string {
	return f.name
}

func (f *_escFile) Size() int64 {
	return f.size
}

func (f *_escFile) Mode() os.FileMode {
	return 0
}

func (f *_escFile) ModTime() time.Time {
	return time.Unix(f.modtime, 0)
}

func (f *_escFile) IsDir() bool {
	return f.isDir
}

func (f *_escFile) Sys() interface{} {
	return f
}

// _escFS returns a http.Filesystem for the embedded assets. If useLocal is true,
// the filesystem's contents are instead used.
func _escFS(useLocal bool) http.FileSystem {
	if useLocal {
		return _escLocal
	}
	return _escStatic
}

// _escDir returns a http.Filesystem for the embedded assets on a given prefix dir.
// If useLocal is true, the filesystem's contents are instead used.
func _escDir(useLocal bool, name string) http.FileSystem {
	if useLocal {
		return _escDirectory{fs: _escLocal, name: name}
	}
	return _escDirectory{fs: _escStatic, name: name}
}

// _escFSByte returns the named file from the embedded assets. If useLocal is
// true, the filesystem's contents are instead used.
func _escFSByte(useLocal bool, name string) ([]byte, error) {
	if useLocal {
		f, err := _escLocal.Open(name)
		if err != nil {
			return nil, err
		}
		b, err := ioutil.ReadAll(f)
		_ = f.Close()
		return b, err
	}
	f, err := _escStatic.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.data, nil
}

// _escFSMustByte is the same as _escFSByte, but panics if name is not present.
func _escFSMustByte(useLocal bool, name string) []byte {
	b, err := _escFSByte(useLocal, name)
	if err != nil {
		panic(err)
	}
	return b
}

// _escFSString is the string version of _escFSByte.
func _escFSString(useLocal bool, name string) (string, error) {
	b, err := _escFSByte(useLocal, name)
	return string(b), err
}

// _escFSMustString is the string version of _escFSMustByte.
func _escFSMustString(useLocal bool, name string) string {
	return string(_escFSMustByte(useLocal, name))
}

var _escData = map[string]*_escFile{

	"/kernels.hsaco": {
		name:    "kernels.hsaco",
		local:   "kernels.hsaco",
		size:    9352,
		modtime: 1564639620,
		compressed: `
H4sIAAAAAAAC/+xa3W/bVBQ/vnFS4xUIjIfBJnH5kCpNjZVmpYr6QpKWlmpp166jo7BpcuMb19QfwXHa
ZoJuIIT6MAkEL7zxD/A3rJF45QHx3Ade9ifAI0G2740/FqetNLSh+SclP/uce77i49bX9977oL6AOK4C
FBn4Ezj3IO+fM8XhWz5f9mRlEKAC4yBCDgD40Lg497goC1TOUbskkPEos3xcu2zoPM6/c1EO27m5Aqby
GLcgyswOndGO1Xf9kaPwp7AL5+di7ZGj5ODs4NnviSBIPMSXxCjzITuBxq8uz3vD2bW56PWDL+dhbFAb
k1WX5xdXP/LHuu1xjsplQ1EbZkE2FPez3ZZdUrW7Db2gNvfLxSvU/48vAIjUplAoiBvEbmuWOYsZPsVT
k7iIb4tXiW0SvR1oRIwLeEU2SCDCGBu2uSe6B+tdY8vSQ/oJV1XZUSY8dV021Y6sBsbXWsScq+O5iHaQ
jpdGCd/2tFVbbUeCij4NSWeDNKoiO7nRbZHIiImmbsnO9OWJwZB17W7UQXmgquqaas4OVW3Ieodc1UyF
qRd1a0vWa51mk9jRUW4ObNTClVLgXVHs9ZbcIGsdWZ8duAj0jQbT+JgnTbmjOyNLrz2/pc89p6XvaYqz
PaLlNdNJLns6uezp5LJrXU+UXPFSuOIzV6SQ3SXlf1zQE2quDzVFIabfH9eazTZxPh6R4Mz0fx9/8ynH
/+QpxF+xzFF9UT7lnZ1mddqs5iyFrNpWa/APn1q5jyOyra4T1SCm4+ddLlLlom11WlS1oO0Txdcz9aqt
7coOSR4QdU4rZ/nelHdJ07ZYUIwHvbbSMdYXV68HzyZTU4FmY7hmWd5f0GXnpmXv+Fl7TkvvzYiSJIkj
nze50OeNBF2KFCmebXB0ish5s7vMiTduDX6B7725XvTvw2ro+Jw3EwzNTXk+1+/3+89i/V8D6vFe5bne
BQD4FVDPnaLfgwdHqI++dbMWkPADB1ASAEoZgBLA4Zef4fsP34HDowyIPdfPz4DeFuC7P74BBBlO7OWp
D8hDiUPogDuPyhnEH6C8UELnxTLArWM/g1vHPADfh/sPASqVLHrt4HPuwRFwgDJjOYSyPOIyCLVcXwJM
8nmhBCKahJdykzDOT4qery+O/fn8V0dpR6dIkSJFihQpUqRIkSJFimFga814nM3dfVygnKWsUL0Yem/g
4q9/+pbLZapn68o3xofHq2vmDrFncb0+j6elIjR02VTxrr/UistSEZ/8vsLNuvJKVD5G5b/F5C+6OaPc
Y/sDXnW/0FiwLs8gmZZDQFK6ZrtrgKSaHWlbbm8D/Xbljg2SQ/Yd70w2tAZIDcswiOmA1O4ajrwFUnu7
7dj+kc9QqxXvTHnfJTBscw/uzG+uVJeX5p7Ye5yx0LJ/0vr/4J0MPP77nQuZsX5gHO4HLrTPgclfBoC/
+32L2bN+YHwplpYQi/869Y1i/cM4H7PnY/wm3ZeAYv3KmBvaRwEmwntEIHlfSZKDArXNMEHCfo9srH4W
Zoa6LMbCtKj9uwnhGb8fvvYh/ETt9yG4v7NDrt9iOPcQ8nTfz+YJv99agr1G7S+eYP9vAAAA//9Ux65h
iCQAAA==
`,
	},
}

var _escDirs = map[string][]os.FileInfo{}
