
#define ALPHA 2
#define BETA 3
#define CacheBlockSize 64

__kernel void mrnw(__global float4 *VecA,
                              __global float4 *VecB,
                              __global float4* VecC,
                              int widthA,
                              int devId)
{
    int gid = get_global_id(0);
    int index = gid + devId;

    if (index < widthA )
        VecC[index] = ALPHA * VecA[index] + BETA * VecB[index];

}