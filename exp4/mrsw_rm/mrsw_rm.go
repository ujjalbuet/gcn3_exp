package mrsw_rm

import (
	"log"
	"math"
	"math/rand"

	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/gcn3/insts"
	"gitlab.com/akita/gcn3/kernels"
)

type KernelArgs1 struct {
	VecA   driver.GPUPtr
	VecB   driver.GPUPtr
	VecC driver.GPUPtr
	Width  int32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64

}

type KernelArgs2 struct {
	VecA   driver.GPUPtr
	VecC1   driver.GPUPtr
	VecC2 driver.GPUPtr
	Width  int32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64

}

type Benchmark struct {
	driver           *driver.Driver
	context          *driver.Context
	//hsaco   *insts.HsaCo
	gpus             []int
	queue           *driver.CommandQueue
	//queue           *driver.CommandQueue
	read_kernel  *insts.HsaCo
	write_kernel *insts.HsaCo
	Width  int

	a, bb, cOutput, c []float32
	dA, dB     driver.GPUPtr
	dC     []driver.GPUPtr
}

func NewBenchmark(driver *driver.Driver) *Benchmark {
	b := new(Benchmark)

	b.driver = driver
	b.context = b.driver.Init()
	b.queue = driver.CreateCommandQueue(b.context)

	//hsacoBytes, err := Asset("/kernels.hsaco")
	//if err != nil {
	//	log.Panic(err)
	//}
	//b.hsaco = kernels.LoadProgramFromMemory(hsacoBytes, "mrnw")

	hsacoBytes := _escFSMustByte(false, "/kernels.hsaco")
	b.read_kernel = kernels.LoadProgramFromMemory(
		hsacoBytes, "mrsw_read")
	if b.read_kernel == nil {
		log.Panic("Failed to load kernel binary")
	}
	b.write_kernel = kernels.LoadProgramFromMemory(
		hsacoBytes, "mrsw_write")
	if b.read_kernel == nil {
		log.Panic("Failed to load kernel binary")
	}
	return b
}


func (b *Benchmark) SelectGPU(gpus []int) {
	b.gpus = gpus
}

//func (b *Benchmark) loadProgram() {
//	hsacoBytes := _escFSMustByte(false, "/kernels.hsaco")
//
//	b.kernel = kernels.LoadProgramFromMemory(
//		hsacoBytes, "mrnw")
//	if b.kernel == nil {
//		log.Panic("Failed to load kernel binary")
//	}
//}



//changed run
func (b *Benchmark) Run() {
	b.driver.SelectGPU(b.context, b.gpus[0])
	b.initMem()
	b.exec()
}


func (b *Benchmark) initMem() {
	rand.Seed(1)
	b.a = make([]float32, b.Width)
	b.bb = make([]float32, b.Width)
	b.c = make([]float32, b.Width)
	b.cOutput = make([]float32, b.Width)

	for i := 0; i < b.Width; i++ {
		b.a[i] = float32(i) * math.Pi
		b.bb[i] = float32(i-1) * math.Pi
		b.c[i] = 0;
	}

	b.dA = b.driver.AllocateMemoryWithAlignment(b.context,
		uint64(b.Width*4), 4096)
	b.dB = b.driver.AllocateMemoryWithAlignment(b.context,
		uint64(b.Width*4), 4096)
	b.dC = make([]driver.GPUPtr, len(b.gpus))
	//b.dC = b.driver.AllocateMemoryWithAlignment(b.context,
	//	uint64(b.Width*4), 4096)
	b.driver.MemCopyH2D(b.context, b.dA, b.a)
	b.driver.MemCopyH2D(b.context, b.dB, b.bb)
	for i, gpu := range b.gpus {
		b.driver.SelectGPU(b.context, gpu)
		b.dC[i] = b.driver.AllocateMemory(
			b.context, uint64(b.Width*4))
		b.driver.MemCopyH2D(b.context, b.dC[i], b.c)
	}


}

func (b *Benchmark) exec() {
				b.read()
				b.write()
}



func (b *Benchmark) read() {

	queues := make([]*driver.CommandQueue, len(b.gpus))
	numWi := b.Width

	for i, gpu := range b.gpus {
		b.driver.SelectGPU(b.context, gpu)
		queues[i] = b.driver.CreateCommandQueue(b.context)
		kernArg1 := KernelArgs1{
			VecA:   b.dA,
			VecB:   b.dB,
			VecC: 	b.dC[i],
			Width:  int32(b.Width),
			HiddenGlobalOffsetX: int64(b.Width),
			HiddenGlobalOffsetY: 0,
			HiddenGlobalOffsetZ: 0,
		}

		b.driver.EnqueueLaunchKernel(
			queues[i],
			b.read_kernel,
			[3]uint32{uint32(numWi / len(b.gpus)), 1, 1},
			[3]uint16{256, 1, 1}, &kernArg1,
		)
	}

	for i := range b.gpus {
		b.driver.DrainCommandQueue(queues[i])
	}

	//b.driver.MemCopyD2H(b.context, b.cOutput, b.dC)
}

func (b *Benchmark) Verify() {
	b.cpuCheck()

//	for i := 0; i < b.Width; i++ {
//		if b.c[i] != b.cOutput[i] {
//			log.Panicf("Mismatch at %d, expected %f, but get %f",
//				i, b.c[i], b.cOutput[i])
//		}
//	}

	log.Printf("Passed!\n")
}


func (b *Benchmark) cpuCheck() {
	b.c = make([]float32, b.Width)

	for i := 0; i < b.Width; i++ {
		b.c[i] = 0
	}

	for i := 0; i < b.Width; i++ {
		b.c[i] = b.a[i] + b.bb[i]

	}
}



func (b *Benchmark) write() {

	queues := make([]*driver.CommandQueue, len(b.gpus))
	numWi := b.Width

	//for i, gpu := range b.gpus {
	for i := 1; i<=1; i++  {
		b.driver.SelectGPU(b.context, 2)
		queues[i] = b.driver.CreateCommandQueue(b.context)
		kernArg2 := KernelArgs2{
			VecA:   b.dA,
			VecC1:   b.dA,
			VecC2: 	b.dC[i],
			Width:  int32(b.Width),
			HiddenGlobalOffsetX: int64(b.Width),
			HiddenGlobalOffsetY: 0,
			HiddenGlobalOffsetZ: 0,
		}

		b.driver.EnqueueLaunchKernel(
			queues[i],
			b.write_kernel,
			[3]uint32{uint32(numWi / len(b.gpus)), 1, 1},
			[3]uint16{256, 1, 1}, &kernArg2,
		)
	}

	for i := range b.gpus {
		b.driver.DrainCommandQueue(queues[i])
	}

	//b.driver.MemCopyD2H(b.context, b.cOutput, b.dC)
}

//	for i := 0; i < b.Width; i++ {
//		if b.c[i] != b.cOutput[i] {
//			log.Panicf("Mismatch at %d, expected %f, but get %f",
//				i, b.c[i], b.cOutput[i])
//		}
//	}
