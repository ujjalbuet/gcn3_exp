
#define ALPHA 2
#define BETA 3
#define CacheBlockSize 64

__kernel void mrnw(__global float4 *VecA,
                              __global float4 *VecB,
                              __global float4* VecC,
                              int widthA,
                              int devId)
{
    int gid = get_global_id(0);
    int index = gid;
    int check_index = index % 16;
    if((devId==0) && (index < widthA)) {
    	if ((check_index == 0) || (check_index == 1) || (check_index == 2) || (check_index  == 3))
        	VecC[index] = ALPHA * VecA[index] + BETA * VecB[index]; }
   
    else if((devId==1) && (index < widthA)) {
    	if ((check_index == 4) || (check_index == 5) || (check_index == 6) || (check_index  == 7))
        	VecC[index] = ALPHA * VecA[index] + BETA * VecB[index]; }
    else if((devId==2) && (index < widthA)) {
    	if ((check_index == 8) || (check_index == 9) || (check_index == 10) || (check_index  == 11))
        	VecC[index] = ALPHA * VecA[index] + BETA * VecB[index]; }
    else if((devId==3) && (index < widthA)) {
    	if ((check_index == 12) || (check_index == 13) || (check_index == 14) || (check_index  == 15))
        	VecC[index] = ALPHA * VecA[index] + BETA * VecB[index]; }
}
