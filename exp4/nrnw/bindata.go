// Code generated by "esc -o bindata.go -pkg mrnw -private kernels.hsaco"; DO NOT EDIT.

package nrnw

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"sync"
	"time"
)

type _escLocalFS struct{}

var _escLocal _escLocalFS

type _escStaticFS struct{}

var _escStatic _escStaticFS

type _escDirectory struct {
	fs   http.FileSystem
	name string
}

type _escFile struct {
	compressed string
	size       int64
	modtime    int64
	local      string
	isDir      bool

	once sync.Once
	data []byte
	name string
}

func (_escLocalFS) Open(name string) (http.File, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	return os.Open(f.local)
}

func (_escStaticFS) prepare(name string) (*_escFile, error) {
	f, present := _escData[path.Clean(name)]
	if !present {
		return nil, os.ErrNotExist
	}
	var err error
	f.once.Do(func() {
		f.name = path.Base(name)
		if f.size == 0 {
			return
		}
		var gr *gzip.Reader
		b64 := base64.NewDecoder(base64.StdEncoding, bytes.NewBufferString(f.compressed))
		gr, err = gzip.NewReader(b64)
		if err != nil {
			return
		}
		f.data, err = ioutil.ReadAll(gr)
	})
	if err != nil {
		return nil, err
	}
	return f, nil
}

func (fs _escStaticFS) Open(name string) (http.File, error) {
	f, err := fs.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.File()
}

func (dir _escDirectory) Open(name string) (http.File, error) {
	return dir.fs.Open(dir.name + name)
}

func (f *_escFile) File() (http.File, error) {
	type httpFile struct {
		*bytes.Reader
		*_escFile
	}
	return &httpFile{
		Reader:   bytes.NewReader(f.data),
		_escFile: f,
	}, nil
}

func (f *_escFile) Close() error {
	return nil
}

func (f *_escFile) Readdir(count int) ([]os.FileInfo, error) {
	if !f.isDir {
		return nil, fmt.Errorf(" escFile.Readdir: '%s' is not directory", f.name)
	}

	fis, ok := _escDirs[f.local]
	if !ok {
		return nil, fmt.Errorf(" escFile.Readdir: '%s' is directory, but we have no info about content of this dir, local=%s", f.name, f.local)
	}
	limit := count
	if count <= 0 || limit > len(fis) {
		limit = len(fis)
	}

	if len(fis) == 0 && count > 0 {
		return nil, io.EOF
	}

	return fis[0:limit], nil
}

func (f *_escFile) Stat() (os.FileInfo, error) {
	return f, nil
}

func (f *_escFile) Name() string {
	return f.name
}

func (f *_escFile) Size() int64 {
	return f.size
}

func (f *_escFile) Mode() os.FileMode {
	return 0
}

func (f *_escFile) ModTime() time.Time {
	return time.Unix(f.modtime, 0)
}

func (f *_escFile) IsDir() bool {
	return f.isDir
}

func (f *_escFile) Sys() interface{} {
	return f
}

// _escFS returns a http.Filesystem for the embedded assets. If useLocal is true,
// the filesystem's contents are instead used.
func _escFS(useLocal bool) http.FileSystem {
	if useLocal {
		return _escLocal
	}
	return _escStatic
}

// _escDir returns a http.Filesystem for the embedded assets on a given prefix dir.
// If useLocal is true, the filesystem's contents are instead used.
func _escDir(useLocal bool, name string) http.FileSystem {
	if useLocal {
		return _escDirectory{fs: _escLocal, name: name}
	}
	return _escDirectory{fs: _escStatic, name: name}
}

// _escFSByte returns the named file from the embedded assets. If useLocal is
// true, the filesystem's contents are instead used.
func _escFSByte(useLocal bool, name string) ([]byte, error) {
	if useLocal {
		f, err := _escLocal.Open(name)
		if err != nil {
			return nil, err
		}
		b, err := ioutil.ReadAll(f)
		_ = f.Close()
		return b, err
	}
	f, err := _escStatic.prepare(name)
	if err != nil {
		return nil, err
	}
	return f.data, nil
}

// _escFSMustByte is the same as _escFSByte, but panics if name is not present.
func _escFSMustByte(useLocal bool, name string) []byte {
	b, err := _escFSByte(useLocal, name)
	if err != nil {
		panic(err)
	}
	return b
}

// _escFSString is the string version of _escFSByte.
func _escFSString(useLocal bool, name string) (string, error) {
	b, err := _escFSByte(useLocal, name)
	return string(b), err
}

// _escFSMustString is the string version of _escFSMustByte.
func _escFSMustString(useLocal bool, name string) string {
	return string(_escFSMustByte(useLocal, name))
}

var _escData = map[string]*_escFile{

	"/kernels.hsaco": {
		name:    "kernels.hsaco",
		local:   "kernels.hsaco",
		size:    9352,
		modtime: 1563952736,
		compressed: `
H4sIAAAAAAAC/+xaXW/b1Bt/fOKk/jv7Q2BcDIaEEUiVpsZKs1JFFRJNWlqmpV26Qkdh0+TGJ46pX4Lj
tM0EXUEI9WIXgEDigo/AZ1gjuOGCK657wc0+AlwSdOxz4pfFbScNhjT/pOZnP8953uwncY/Puft2fQlx
3DxQZOB34MhBwT9nisNXfb7kySogwDycAxFyAMCHxsV5wEVZoHKO2iXhu3yUWT7ELhs6j/OvXJTDdiRX
kKg8xh2IMrNDj2jH6rv+wFX5M9iF8yNYe+CqOXh08Ox6IggSD3H9f1HmQ3YCjV9dWfSGs3tz0esHX87D
xKg2JquuLC433vPHkvbIU7liqlrTKiqmSv7aXYWQpt9pGkWttVcpXab+fxIARGpTLBbFDex0dduakxg+
lKanpJJ0S7yKHQsb3UAjSlJRWlVMHIgkSTIda1ckB+t9c8s2QvpJoprfVic9dV2xtJ6iBcbXOthaqEsL
Ee0oHS+NsnTL01YdrRsJKvo0Jp0N3KyK7OTdfgdHRky2DFtxZy5Njoas63eiDiojVdXQNWturGpDMXr4
qm6pTL1s2FuKUeu1WtiJjiI5sFFLl8uBd1V11jtKE6/1FGNu5CLQN5tM42MRt5Se4Z5Yeu3pLX3hKS19
V1fd9gktr1tuctkzyWXPJJdd63ui5IqvhCs+raLHdC/e0VUVW/7lvNZqdbH7/gkJzs788/E3n3D8D55A
/FXbOqkvKmf8IqRZnTWrBVvFDcfujJ6P1Io8vRVHW8eaiS3Xz7tSosplx+51qGpJ38Oqr2fqhqPvKC5O
HhB1Titn+d5QdnDLsVlQSRr12mrPXF9uXA8e5dPTgWZjvGZF2VsyFPeG7Wz7WXtOy2/MirIsiyf/f8aF
/l5K0KVIkSLFvwmOThE5b3aXOfWHqAY/wlfeXC/6g9cIHechH52b8nxuOBwO/4v1fw5oQOaiPwMaXPCu
QG5Apuh34d4RGnJfkqwFTvhGACgDoHIGHX76kXRw/zU4PMqAOCA+DhDsC/D1bz8AggwnDgrUHgpQ5hDa
586jSgbx+6gglNF5sQJw89iPfvOYB+CHcHAfYH4+i17Y/5i7dwQcoMxEDqEsj7gMQh3iS4ApviCUQURT
8ExuCs7xU6Ln65Njfy7/2VHazSlSpEiRIkWKFClSpEiRIglsrbmRZ3N3HxcoZyl/SxVi6L0BwR9/DW3C
bapn68pf5MfHq+vWNnbmpHp9UZqRS9A0FEuTdvylVqkil6TT31eQrN98LiqfoPJfYvL/k5xR7qH9Ac+T
DzQRrMszyJbtYpDVvtXtmyBrVk9uK9020E8idx2QXbznemeKqTdBbtqmiS0X5G7fdJUtkLvtruv4Rz5D
rVa6Pe19lsF0rF24vbi5Wl25svDY3uNMhJb9k9b/R+9k4OHrlw+ZsX5gHO4HLrTPgcmfBYA/h0Ob2bN+
YPxyLC0hFv9F6hvF+odxIWbPx/gVui8BxfqVMTe2jwJMhveIQPK+kiQHRWqbYYKE/R7ZWP0szCx1WYqF
6VD71xPCM34rfO9D+J7a70Hw/c6OuX/L4dxDKNB9P5unXL+1BHud2l88xf7vAAAA///XDyU4iCQAAA==
`,
	},
}

var _escDirs = map[string][]os.FileInfo{}
