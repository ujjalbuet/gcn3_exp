
#define ALPHA 2
#define BETA 3

__kernel void mrnw(__global float4 *VecA,
                              __global float4 *VecB,
                              __global float4* VecC,
                              int widthA)
{
    int gid = get_global_id(0);

    if (gid < widthA)
        VecC[gid] = ALPHA * VecA[gid] + BETA * VecB[gid];

}