#!/bin/bash

git clone https://gitlab.com/syifan/multi_gpu_experiments.git
cd multi_gpu_experiments/discrete
go build

echo "1-GPU Execution"
./discrete -benchmark=fir -num-gpus=1

echo "2-GPU Execution"
./discrete -benchmark=fir -num-gpus=2

echo "4-GPU Execution"
./discrete -benchmark=fir -num-gpus=4

#test this